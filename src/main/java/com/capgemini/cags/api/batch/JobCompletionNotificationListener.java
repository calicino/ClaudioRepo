package com.capgemini.cags.api.batch;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Execute post-job check to verify correct API publishing.
 * 
 * @author vquartar
 *
 */
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);
	
	@Autowired
	private Configuration config;
	
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private AccessTokenResponse accessToken;
	
	@Autowired
	private FlatFileItemReader<SOAPService> reader;

	@Override
	public void afterJob(JobExecution jobExecution) {
		if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
			reader.open(new ExecutionContext());
			log.info("!!! JOB FINISHED! Time to verify the results");
			SOAPService service = null;
			final String baseUrl = "https://"+config.getApimHost()+":"+config.getApimPort();
			HttpHeaders tokenHeaders = new HttpHeaders();
			tokenHeaders.add("Authorization", accessToken.getToken_type() + " " + accessToken.getAccess_token());
			tokenHeaders.setContentType(MediaType.APPLICATION_JSON);
			try {
				while ((service = reader.read()) != null) {
					log.debug("checking correct publishing of service name: {}", service.getName());
					String searchAPIuri = baseUrl+"/api/am/publisher/v0.11/apis?query=name:"+service.getName();
					HttpEntity <String> searchAPIRequestHttpEntity = new HttpEntity <String> (tokenHeaders);
					ResponseEntity<String> searchAPIResponse = restTemplate.exchange(searchAPIuri, HttpMethod.GET, searchAPIRequestHttpEntity, String.class);
					JSONObject jsonObj = new JSONObject(searchAPIResponse.getBody());
					int count = (int) jsonObj.get("count");
					if (count >= 1) {
						JSONArray apiList = (JSONArray) jsonObj.get("list");
						for (int i = 0; i < count; i++) {
							JSONObject api = (JSONObject) apiList.get(i);
							if (api.getString("name").equals(service.getName())) {
								if (api.getString("status").equals("PUBLISHED")) {
									log.info("verified api {} in status PUBLISHED. id={}", service.getName(), api.getString("id"));
								} else {
									log.error("api {} in status {}", service.getName(), api.getString("status"));
								}
								break;
							}
						}
					} else {
						log.error("api {} not present in gateway", service.getName());
					}
				}
			} catch (Exception e) {
				log.error("Errore in fase di verifica dei risultati: {}", e.getMessage());
			}
		}
	}
}