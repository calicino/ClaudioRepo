package com.capgemini.cags.api.batch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class holds configuration parameters by reading command line arguments.
 * 
 * @author vquartar
 *
 */
@Component
public class Configuration {
	
	@Value("${wso2-username:admin}")
	private String username;
	
	@Value("${password:admin}")
	private String password;

	@Value("${inputFile}")
	private String inputFile;
	
	@Value("${outputFile}")
	private String outputFile;
	
	@Value("${inputFields:{\"name\", \"description\", \"context\", \"endpoint\"}}")
	private String[] inputFields;
	
	@Value("${host:localhost}")
	private String apimHost;
	
	@Value("${port:31443}")
	private String apimPort;
	
	@Value("${token-port:31243}")
	private String apimTokenPort;
	
	@Value("${tag:null}")
	private String tag;
	
	@Value("${sleep:5}")
	private int sleep;
	
	@Value("${threads:10}")
	private int threads;
	
	@Value("${chunk-size:10}")
	private int chunkSize;

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	
	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public String[] getInputFields() {
		return inputFields;
	}

	public void setInputFields(String[] inputFields) {
		this.inputFields = inputFields;
	}

	public String getApimHost() {
		return apimHost;
	}

	public void setApimHost(String apimHost) {
		this.apimHost = apimHost;
	}

	public String getApimPort() {
		return apimPort;
	}

	public void setApimPort(String apimPort) {
		this.apimPort = apimPort;
	}

	public String getApimTokenPort() {
		return apimTokenPort;
	}

	public void setApimTokenPort(String apimTokenPort) {
		this.apimTokenPort = apimTokenPort;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public void setChunkSize(int chunkSize) {
		this.chunkSize = chunkSize;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag=tag;
	}
	
	public int getSleep() {
		return sleep;
	}
	
	public void setSleep(int sleep) {
		this.sleep=sleep;
	}
	
}
